
using System;
using System.Drawing;
using System.Threading;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace SplashTest
{
	public partial class SplashScreen : UIViewController
	{
		public event Action OnReady;

		public SplashScreen () : base ("SplashScreen", null)
		{
		}
		
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			aiSplash.StartAnimating();
			
			ThreadPool.QueueUserWorkItem (o => heavyCode());
			// Perform any additional setup after loading the view, typically from a nib.
		}

		object heavyCode ()
		{
			Thread.Sleep(2000);
			InvokeOnMainThread(() => {
				aiSplash.StopAnimating();
				OnReady.Invoke();
			});

			return null;
		}

		public override void ViewDidUnload ()
		{
			base.ViewDidUnload ();
			
			// Clear any references to subviews of the main view in order to
			// allow the Garbage Collector to collect them sooner.
			//
			// e.g. myOutlet.Dispose (); myOutlet = null;
			
			ReleaseDesignerOutlets ();
		}
		
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			// Return true for supported orientations
			return (toInterfaceOrientation != UIInterfaceOrientation.PortraitUpsideDown);
		}
	}
}

