using System;

using MonoTouch.UIKit;

namespace SplashTest
{
	public static class App
	{
		public static AppDelegate Delegate { get { return (AppDelegate)UIApplication.SharedApplication.Delegate; } }
	}
}

