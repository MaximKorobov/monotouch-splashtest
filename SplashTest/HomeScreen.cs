
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace SplashTest
{
	public partial class HomeScreen : UIViewController
	{
		SplashScreen splash;
		bool showingSplashEarlier = false;

		public HomeScreen () : base ("HomeScreen", null)
		{
		}
		
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Perform any additional setup after loading the view, typically from a nib.
		}

		#region Modal view controller methods
		public void ShowModalVC(UIViewController modalViewController, bool animated = true)
		{
			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0)) {
				PresentViewController(modalViewController, animated, null);
			} else {
				PresentModalViewController(modalViewController, animated);
			}
		}

		public void HideModalVC(bool animated = true)
		{
			if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0)) {
				DismissViewController(animated, null);
			} else {
				DismissModalViewControllerAnimated(animated);
			}
		}
		#endregion

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);

			if (!showingSplashEarlier) {
				showingSplashEarlier = true;
				splash = new SplashScreen();
				splash.OnReady += () => {
					HideModalVC(true);
				};
				// Extension method. For more info look at App.cs
				ShowModalVC(splash, false);
			}
		}

		public override void ViewDidUnload ()
		{
			base.ViewDidUnload ();
			
			// Clear any references to subviews of the main view in order to
			// allow the Garbage Collector to collect them sooner.
			//
			// e.g. myOutlet.Dispose (); myOutlet = null;
			
			ReleaseDesignerOutlets ();
		}
		
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			// Return true for supported orientations
			return (toInterfaceOrientation != UIInterfaceOrientation.PortraitUpsideDown);
		}
	}
}

